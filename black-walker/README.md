Black Walker
===

Black Walker is a dream diary application which aims to provide sophisticated means for lucid dreamers to investigate their dreams.

It provides a searchable dream database and specially focuses on _Tags_, small additional information to a dream like who appeared in it and where the plot took place.

Details can be found at the source code repository [https://gitlab.com/Nuramon27/black-walker].

## License

Black Walker is licensed under the GNU General Public License version 3 or later (see [LICENSE](LICENSE) or https://www.gnu.org/licenses/ ).
